package com.ohos.magnetx;

import ohos.data.preferences.Preferences;

public class NotEqual extends Operator{
    private static NotEqual INSTANCE;

    private NotEqual(){
        super();
    }

    public static NotEqual getInstance(){
        synchronized (NotEqual.class){
            if (INSTANCE == null){
                INSTANCE = new NotEqual();
            }
        }
        return INSTANCE;
    }

    @Override
    boolean apply(String key, boolean operand, Preferences preferences) {
        return preferences.getBoolean(key, false) != operand;
    }
}

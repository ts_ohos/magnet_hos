package com.ohos.magnetx;

import ohos.data.preferences.Preferences;

public abstract class Operator {
    public Operator(){}

    abstract boolean apply(String key, boolean operand, Preferences preferences);
}

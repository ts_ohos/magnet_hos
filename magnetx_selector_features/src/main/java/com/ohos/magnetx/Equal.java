package com.ohos.magnetx;

import ohos.data.preferences.Preferences;

public class Equal extends Operator{
    private static Equal INSTANCE;

    private Equal(){
        super();
    }

    public static Equal getInstance(){
        synchronized (Equal.class){
            if (INSTANCE == null){
                INSTANCE = new Equal();
            }
        }
        return INSTANCE;
    }

    @Override
    boolean apply(String key, boolean operand, Preferences preferences) {
        return preferences.getBoolean(key, false) == operand;
    }
}

package com.openharmony.magnet.slice;

import com.openharmony.magnet.ResourceTable;
import com.openharmony.magnet.media.MediaPlayer;
import magnet.Magnet;
import magnet.Scope;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.utils.net.Uri;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        try {
            testMagnet();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    public void testMagnet() throws InterruptedException {
        Scope rootScope = Magnet.createRootScope();
        Scope playerScope  = rootScope.createSubscope();
        playerScope.bind(Uri.class, Uri.parse("https://my-media-file"));

        MediaPlayer player = playerScope.getSingle(MediaPlayer.class);
        player.playWhenReady();

        Thread.sleep(5000);
        playerScope.dispose();
    }
}

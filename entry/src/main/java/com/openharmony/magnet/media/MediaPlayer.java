package com.openharmony.magnet.media;

public interface MediaPlayer {
    void playWhenReady();
}

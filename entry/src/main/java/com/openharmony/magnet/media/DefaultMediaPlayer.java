package com.openharmony.magnet.media;

import magnet.Instance;
import ohos.utils.net.Uri;

@Instance(type = MediaPlayer.class, disposer = "dispose")
public class DefaultMediaPlayer implements MediaPlayer{

    private Uri assetId;
    private MediaLoader mediaLoader;

    public DefaultMediaPlayer(Uri assetId, MediaLoader mediaLoader) {
        this.assetId = assetId;
        this.mediaLoader = mediaLoader;
    }


    @Override
    public void playWhenReady() {
        System.out.println("~~~~~~~~~~~~~~~ play when ready ~~~~~~~~~~~~~~~~~~~~~~" + assetId.toString());
    }

    public void dispose() {
        System.out.println("~~~~~~~~~~~~~~~ play dispose ~~~~~~~~~~~~~~~~~~~~~~");
    }
}

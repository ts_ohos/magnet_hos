package com.openharmony.magnet.media;

import ohos.utils.net.Uri;

public interface MediaLoader {

    String load(Uri mediaUri);
}

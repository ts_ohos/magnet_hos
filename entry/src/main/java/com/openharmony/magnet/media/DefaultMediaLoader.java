package com.openharmony.magnet.media;

import magnet.Instance;
import ohos.utils.net.Uri;

@Instance(type = MediaLoader.class)
public class DefaultMediaLoader implements MediaLoader {

    @Override
    public String load(Uri mediaUri) {
        return mediaUri.toString();
    }
}

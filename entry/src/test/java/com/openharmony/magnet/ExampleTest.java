package com.openharmony.magnet;

import com.openharmony.magnet.media.MediaPlayer;
import magnet.Magnet;
import magnet.Scope;
import ohos.utils.net.Uri;
import org.junit.Test;

public class ExampleTest {

    @Test
    public void testMagnet() {
        Scope rootScope = Magnet.createRootScope();
        Scope subscope = rootScope.createSubscope();
        subscope.getSingle(String.class);
    }
}

package com.ohos.magnetx;

import magnet.Instance;

import java.util.List;

@Instance(type = Delegate.class)
public class Delegate {
    private List<AppExtension> appExtensions;
    public Delegate(List<AppExtension> appExtensions){
        this.appExtensions = appExtensions;
    }

    public void onCreate() {
        for (AppExtension appExtension : appExtensions) {
            appExtension.onCreate();
        }
    }

    public void onTrimMemory(int level) {
        for (AppExtension appExtension : appExtensions) {
            appExtension.onTrimMemory(level);
        }
    }
}

package magnet.processor;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.CompilationSubject;
import com.google.testing.compile.Compiler;
import com.google.testing.compile.JavaFileObjects;
import org.junit.Test;

import javax.annotation.processing.Processor;
import javax.tools.JavaFileObject;

public class InstanceIndexProcessorTest {

    private JavaFileObject withResource(String name){
        return JavaFileObjects.forResource(this.getClass().getSimpleName() + '/' + name);
    }

    @Test
    public void InstanceFactory_index_gets_generated_with_classifier() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor)(new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface1.java"), this.withResource("Implementation1.java")});
        CompilationSubject.assertThat(compilation).succeeded();
        CompilationSubject.assertThat(compilation).generatedSourceFile("magnet.index/app_test_Implementation1MagnetFactory").hasSourceEquivalentTo(this.withResource("generated/app_test_Implementation1MagnetFactory.java"));
    }

    @Test
    public void InstanceFactory_index_gets_generated_without_classifier/* $FF was: InstanceFactory index gets generated without classifier*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor)(new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface2.java"), this.withResource("Implementation2.java")});
        CompilationSubject.assertThat(compilation).succeeded();
        CompilationSubject.assertThat(compilation).generatedSourceFile("magnet.index/app_test_Implementation2MagnetFactory").hasSourceEquivalentTo(this.withResource("generated/app_test_Implementation2MagnetFactory.java"));
    }

}

package magnet.processor;

import com.google.testing.compile.Compilation;
import org.junit.Test;
import com.google.testing.compile.Compiler;
import com.google.testing.compile.JavaFileObjects;
import javax.tools.JavaFileObject;

import static com.google.testing.compile.CompilationSubject.assertThat;

public class MagnetProcessorTest {

    @Test
    public void generateFactory_NoParams() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("HomePageNoParams.java"),
                        withResource("Page.java")
                );

        assertThat(compilation).succeededWithoutWarnings();

        assertThat(compilation)
                .generatedSourceFile("app/extension/HomePageNoParamsMagnetFactory")
                .hasSourceEquivalentTo(withResource("generated/HomePageNoParamsMagnetFactory.java"));
    }

    private JavaFileObject withResource(String name) {
        return JavaFileObjects.forResource(MagnetProcessorTest.class.getSimpleName() + '/' + name);
    }

    @Test
    public void generateFactory_WithScope() {
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("HomePageWithScope.java"),
                        withResource("Page.java")
                );

        assertThat(compilation).succeededWithoutWarnings();

        assertThat(compilation)
                .generatedSourceFile("app/extension/HomePageWithScopeMagnetFactory")
                .hasSourceEquivalentTo(withResource("generated/HomePageWithScopeMagnetFactory.java"));
    }

    @Test
    public void generateFactory_WithArbitraryParams() {
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("HomePageWithParams.java"),
                        withResource("Page.java"),
                        withResource("HomeRepository.java"),
                        withResource("UserData.java")
                );

        assertThat(compilation).succeededWithoutWarnings();

        assertThat(compilation)
                .generatedSourceFile("app/extension/HomePageWithParamsMagnetFactory")
                .hasSourceEquivalentTo(withResource("generated/HomePageWithParamsMagnetFactory.java"));
    }

    @Test
    public void generateFactory_WithArbitraryParamsAndScope() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("HomePage.java"),
                        withResource("Page.java"),
                        withResource("HomeRepository.java"),
                        withResource("UserData.java")
                );

        assertThat(compilation).succeededWithoutWarnings();

        assertThat(compilation)
                .generatedSourceFile("app/extension/HomePageMagnetFactory")
                .hasSourceEquivalentTo(withResource("generated/HomePageMagnetFactory.java"));
    }

    @Test
    public void generateFactory_FailsOnGenericTypeInConstructorParameter() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("HomePageWithGenericParam.java"),
                        withResource("Page.java")
                );

        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("is specified using a generic type");
    }

    @Test
    public void generateFactory_TypeNotImplemented() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("Tab.java"),
                        withResource("UnimplementedTab.java")
                );

        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("must implement");
    }

    @Test
    public void generateFactory_DisabledAnnotation() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("Tab.java"),
                        withResource("DisabledTab.java")
                );

        assertThat(compilation).succeeded();
        com.google.common.truth.Truth.assertThat(compilation.generatedFiles().size()).isEqualTo(2);
    }

    @Test
    public void generateFactory_WithClassifierParams() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("HomePageWithClassifierParams.java"),
                        withResource("Page.java"),
                        withResource("HomeRepository.java"),
                        withResource("UserData.java")
                );

        assertThat(compilation).succeededWithoutWarnings();

        assertThat(compilation)
                .generatedSourceFile("app/extension/HomePageWithClassifierParamsMagnetFactory")
                .hasSourceEquivalentTo(withResource("generated/HomePageWithClassifierParamsMagnetFactory.java"));
    }

    @Test
    public void generateFactory_WithManyParams() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("HomePageWithManyParams.java"),
                        withResource("Page.java"),
                        withResource("HomeRepository.java")
                );

        assertThat(compilation).succeededWithoutWarnings();

        assertThat(compilation)
                .generatedSourceFile("app/extension/HomePageWithManyParamsMagnetFactory")
                .hasSourceEquivalentTo(withResource("generated/HomePageWithManyParamsMagnetFactory.java"));
    }

    @Test
    public void generateFactory_WithManyParameterizedParams() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("HomePageWithManyParameterizedParams.java"),
                        withResource("Page.java"),
                        withResource("WorkProcessor.java")
                );

        assertThat(compilation).succeededWithoutWarnings();

        assertThat(compilation)
                .generatedSourceFile("app/extension/HomePageWithManyParameterizedParamsMagnetFactory")
                .hasSourceEquivalentTo(withResource("generated/HomePageWithManyParameterizedParamsMagnetFactory.java"));
    }

    @Test
    public void generateFactory_WithManyParameterizedWildcardOutParams() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("HomePageWithManyParameterizedWildcardOutParams.java"),
                        withResource("Page.java"),
                        withResource("WorkProcessor.java")
                );

        assertThat(compilation).succeededWithoutWarnings();

        assertThat(compilation)
                .generatedSourceFile("app/extension/HomePageWithManyParameterizedWildcardOutParamsMagnetFactory")
                .hasSourceEquivalentTo(withResource("generated/HomePageWithManyParameterizedWildcardOutParamsMagnetFactory.java"));
    }

    @Test
    public void generateFactory_WithManyParameterizedWildcardInParams() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("HomePageWithManyParameterizedWildcardInParams.java"),
                        withResource("Page.java"),
                        withResource("WorkProcessor.java")
                );

        assertThat(compilation).succeededWithoutWarnings();

        assertThat(compilation)
                .generatedSourceFile("app/extension/HomePageWithManyParameterizedWildcardInParamsMagnetFactory")
                .hasSourceEquivalentTo(withResource("generated/HomePageWithManyParameterizedWildcardInParamsMagnetFactory.java"));
    }

    @Test
    public void generateFactory_WithManyParameterizedWildcardKnownParams() {

        // This is what Kotlin provides to annotation processor, when Kotlin generics are used as parameters

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("HomePageWithManyParameterizedWildcardKnownParams.java"),
                        withResource("Page.java"),
                        withResource("WorkProcessor.java")
                );

        assertThat(compilation).succeededWithoutWarnings();

        assertThat(compilation)
                .generatedSourceFile("app/extension/HomePageWithManyParameterizedWildcardKnownParamsMagnetFactory")
                .hasSourceEquivalentTo(withResource("generated/HomePageWithManyParameterizedWildcardKnownParamsMagnetFactory.java"));
    }

    @Test
    public void generateFactory_WithManyWildcardParams() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("HomePageWithManyWildcardParams.java"),
                        withResource("Page.java"),
                        withResource("HomeRepository.java")
                );

        assertThat(compilation).succeededWithoutWarnings();

        assertThat(compilation)
                .generatedSourceFile("app/extension/HomePageWithManyWildcardParamsMagnetFactory")
                .hasSourceEquivalentTo(withResource("generated/HomePageWithManyWildcardParamsMagnetFactory.java"));
    }

    @Test
    public void generateFactory_UsingStaticMethod() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("HomePageWithStaticConstructor.java"),
                        withResource("HomePageWithStaticConstructorSingle.java"),
                        withResource("Page.java"),
                        withResource("HomeRepository.java")
                );

        assertThat(compilation).succeededWithoutWarnings();

        assertThat(compilation)
                .generatedSourceFile("app/extension/utils/HomePageWithStaticConstructorSingleCreateRepositoriesMagnetFactory")
                .hasSourceEquivalentTo(withResource("generated/HomePageWithStaticConstructorSingleCreateRepositoriesMagnetFactory.java"));
    }

    @Test
    public void generateFactory_StaticMethodProvidesInnerClass() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("StaticMethodProvidesInnerClass/PowerManager.java"),
                        withResource("StaticMethodProvidesInnerClass/PowerManagerProvider.java")
                );

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/PowerManagerProviderProvideWakeLockMagnetFactory")
                .hasSourceEquivalentTo(withResource("StaticMethodProvidesInnerClass/expected/PowerManagerProviderProvideWakeLockMagnetFactory.java"));
    }

    @Test
    public void generateFactory_Covariance_Constructor_ManyParameter() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("Covariance_Constructor_ManyParameter/Foo.java"),
                        withResource("Covariance_Constructor_ManyParameter/UnderTest.java")
                );

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource("Covariance_Constructor_ManyParameter/expected/UnderTestMagnetFactory.java"));
    }

    @Test
    public void generateFactory_Covariance_Constructor_SingleParameter() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("Covariance_Constructor_SingleParameter/Foo.java"),
                        withResource("Covariance_Constructor_SingleParameter/UnderTest.java")
                );

        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("is specified using a generic type");
    }

//    @Test
    //TODO java不支持lazy
    public void generateFactory_Lazy_Constructor_NoKotlinMetadata() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource("Lazy_Constructor_NoKotlinMetadata/UnderTest.java"));

        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("can only be used with Kotlin classes");
    }

//    @Test
    //TODO java不支持lazy
    public void generateFactory_Lazy_Constructor_OptionalParameter() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource("Lazy_Constructor_OptionalParameter/UnderTest.java"));

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource("Lazy_Constructor_OptionalParameter/expected/UnderTestMagnetFactory.java"));
    }

//    @Test
    //TODO java不支持lazy
    public void generateFactory_Lazy_Constructor_OptionalParameter_Wildcard() {
        String root = "Lazy_Constructor_OptionalParameter_Wildcard";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource(root + "/UnderTest.java"));

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(root + "/expected/UnderTestMagnetFactory.java"));
    }

//    @Test
    //TODO java不支持lazy
    public void generateFactory_Lazy_Constructor_SingleParameter() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource("Lazy_Constructor_SingleParameter/UnderTest.java"));

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource("Lazy_Constructor_SingleParameter/expected/UnderTestMagnetFactory.java"));
    }

//    @Test
    //TODO java不支持lazy
    public void generateFactory_Lazy_Constructor_SingleParameter_Wildcard() {
        String root = "Lazy_Constructor_SingleParameter_Wildcard";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource(root + "/UnderTest.java"));

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(root + "/expected/UnderTestMagnetFactory.java"));
    }

//    @Test
    //TODO java不支持lazy
    public void generateFactory_Lazy_Constructor_ManyParameter() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource("Lazy_Constructor_ManyParameter/UnderTest.java"));

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource("Lazy_Constructor_ManyParameter/expected/UnderTestMagnetFactory.java"));
    }

//    @Test
    //TODO java不支持lazy
    public void generateFactory_Lazy_Constructor_ManyParameter_Wildcard() {
        String root = "Lazy_Constructor_ManyParameter_Wildcard";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource(root + "/UnderTest.java"));

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(root + "/expected/UnderTestMagnetFactory.java"));
    }

//    @Test
    //TODO java不支持lazy
    public void generateFactory_Lazy_Constructor_ManyParameter_NullableGenericType() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource("Lazy_Constructor_ManyParameter_NullableGenericType/UnderTest.java"));

        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("be parametrized with none nullable type");
    }

//    @Test
    //TODO java不支持lazy
    public void generateFactory_Lazy_Constructor_ManyParameter_NullableListType() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource("Lazy_Constructor_ManyParameter_NullableListType/UnderTest.java"));

        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("be parametrized with none nullable List type");
    }

//    @Test
    //TODO java不支持lazy
    public void generateFactory_Lazy_Method_SingleParameter() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource("Lazy_Method_SingleParameter/UnderTest.java"));

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestProvideUnderTestDepMagnetFactory")
                .hasSourceEquivalentTo(withResource("Lazy_Method_SingleParameter/expected/UnderTestProvideUnderTestDepMagnetFactory.java"));
    }

//    @Test
    //TODO java不支持lazy
    public void generateFactory_Lazy_Method_SingleParameter_Wildcard() {
        String root = "Lazy_Method_SingleParameter_Wildcard";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource(root + "/UnderTest.java"));

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestProvideUnderTestDepMagnetFactory")
                .hasSourceEquivalentTo(withResource(root + "/expected/UnderTestProvideUnderTestDepMagnetFactory.java"));
    }

    @Test
    public void generateFactory_Generics_ProvideTypeWithParameter() {

        String path = "Generics_ProvideTypeWithParameter";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/UnderTest.java"),
                        withResource(path + "/Type.java"),
                        withResource(path + "/Parameter.java")
                );

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestProvideTypeMagnetFactory")
                .hasSourceEquivalentTo(withResource(path + "/expected/UnderTestProvideTypeMagnetFactory.java"));
    }

    @Test
    public void generateFactory_Generics_ProvideTypeWithParameter_NoClassifier() {

        String path = "Generics_ProvideTypeWithParameter_NoClassifier";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/UnderTest.java"),
                        withResource(path + "/Type.java"),
                        withResource(path + "/Parameter.java")
                );

        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("must have 'classifier' value");
    }

//    @Test
    //TODO java不支持lazy
    public void generateFactory_Lazy_Constructor_SingleParameter_ParameterizedType() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("Lazy_Constructor_SingleParameter_ParameterizedType/Foo.java"),
                        withResource("Lazy_Constructor_SingleParameter_ParameterizedType/UnderTest.java")
                );

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource("Lazy_Constructor_SingleParameter_ParameterizedType/expected/UnderTestMagnetFactory.java"));
    }

//    @Test
    //TODO java不支持lazy
    public void generateFactory_Lazy_Method_OptionalParameter() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource("Lazy_Method_OptionalParameter/UnderTest.java"));

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestProvideUnderTestDepMagnetFactory")
                .hasSourceEquivalentTo(withResource("Lazy_Method_OptionalParameter/expected/UnderTestProvideUnderTestDepMagnetFactory.java"));
    }

//    @Test
    //TODO java不支持lazy
    public void generateFactory_Lazy_Method_OptionalParameter_Wildcard() {
        String root = "Lazy_Method_OptionalParameter_Wildcard";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource(root + "/UnderTest.java"));

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestProvideUnderTestDepMagnetFactory")
                .hasSourceEquivalentTo(withResource(root + "/expected/UnderTestProvideUnderTestDepMagnetFactory.java"));
    }

    @Test
    public void generateFactory_Limit_NotEmpty_GenerateGetter() {
        String root = "Limit_NotEmpty_HasGetter";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource(root + "/UnderTest.java"));

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(root + "/expected/UnderTestMagnetFactory.java"));
    }

    @Test
    public void generateFactory_Limit_Empty_NoGetter() {
        String root = "Limit_Empty_NoGetter";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource(root + "/UnderTest.java"));

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(root + "/expected/UnderTestMagnetFactory.java"));
    }

    @Test
    public void generateFactory_Limit_ReservedAsterix_Fails() {
        String root = "Limit_ReservedAsterisks_Fails";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource(root + "/UnderTest.java"));

        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("Use another value");
    }

    @Test
    public void generateFactory_Limit_ScopingDirect_GeneratesGetter() {
        String root = "Limit_ScopingDirect_GeneratesGetter";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource(root + "/UnderTest.java"));

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(root + "/expected/UnderTestMagnetFactory.java"));
    }

    @Test
    public void generateFactory_Limit_ScopingUnscoped_Fails() {
        String root = "Limit_ScopingUnscoped_Fails";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource(root + "/UnderTest.java"));

        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("Limit can only be used with Scoping.TOPMOST");
    }

//    @Test
    //TODO java不支持lazy
    public void generateFactory_Lazy_Method_NoKotlinMetadata() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(withResource("Lazy_Method_NoKotlinMetadata/UnderTest.java"));

        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("can only be used with Kotlin classes");
    }

    @Test
    public void generateFactory_ScopeParameter_CustomName() {

        String path = "ScopeParameter_CustomName";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/UnderTest.java")
                );

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(path + "/expected/UnderTestMagnetFactory.java"));
    }

//    @Test
    //TODO 不支持
    public void generateFactory_ScopeParameter_CustomName_KotlinClass() {

        String path = "ScopeParameter_CustomName_KotlinClass";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/UnderTest.java")
                );

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(path + "/expected/UnderTestMagnetFactory.java"));
    }

    @Test
    public void generateFactory_ScopeParameter_DefaultName() {

        String path = "ScopeParameter_DefaultName";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/UnderTest.java")
                );

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(path + "/expected/UnderTestMagnetFactory.java"));
    }

//    @Test
    //TODO 不支持 kotlin.Metadata
    public void generateFactory_DefaultArguments_JvmOverloads_AtTheEnd() {

        String path = "DefaultArguments_JvmOverloads_AtTheEnd";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/UnderTest.java")
                );

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(path + "/expected/UnderTestMagnetFactory.java"));
    }

//    @Test
    //TODO 不支持 kotlin.Metadata
    public void generateFactory_DefaultArguments_JvmOverloads_InTheMiddle() {

        String path = "DefaultArguments_JvmOverloads_InTheMiddle";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path+"/UnderTest.java")
                );

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(path + "/expected/UnderTestMagnetFactory.java"));
    }

//    @Test
    //TODO 不支持 kotlin.Metadata
    public void generateFactory_DefaultArguments_JvmOverloads_Mixed() {

        String path = "DefaultArguments_JvmOverloads_Mixed";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/UnderTest.java")
                );

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(path + "/expected/UnderTestMagnetFactory.java"));
    }

    @Test
    public void generateFactory_StaticMethodNeedsDependencyWithClassifier() {
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("StaticMethodNeedsDependencyWithClassifier/Constants.java"),
                        withResource("StaticMethodNeedsDependencyWithClassifier/Input.java"),
                        withResource("StaticMethodNeedsDependencyWithClassifier/Output.java"),
                        withResource("StaticMethodNeedsDependencyWithClassifier/StaticFunction.java")
                );

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/StaticFunctionProvideInputMagnetFactory")
                .hasSourceEquivalentTo(withResource("StaticMethodNeedsDependencyWithClassifier/generated/StaticFunctionProvideInputMagnetFactory.java"));
    }

    @Test
    public void generateFactory_DisabledAnnotation_UsingStaticMethod() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("HomePageWithStaticConstructor.java"),
                        withResource("HomePageWithStaticConstructorDisabled.java"),
                        withResource("Page.java"),
                        withResource("HomeRepository.java")
                );

        assertThat(compilation).succeededWithoutWarnings();
        com.google.common.truth.Truth.assertThat(compilation.generatedFiles().size()).isEqualTo(4);
    }

    @Test
    public void generateFactoryIndex_ForInterfaceWithGenericType() {

        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource("Executor.java"),
                        withResource("ExecutorImpl.java"),
                        withResource("AppExtensionRegistry.java")
                );

        assertThat(compilation)
                .generatedSourceFile("app/extension/ExecutorImplMagnetFactory")
                .hasSourceEquivalentTo(withResource("generated/ForInterfaceWithGenericType_ExecutorMagnetFactory.java"));
    }

    @Test
    public void generateFactory_Constructor_Public_PackagePrivate() {

        String path = "Constructor_Public_PackagePrivate";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/UnderTest.java")
                );

        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("must have exactly one public or package-private constructor");
    }

    @Test
    public void generateFactory_Constructor_Public_Public() {

        String path = "Constructor_Public_Public";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/UnderTest.java")
                );

        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("must have exactly one public or package-private constructor");
    }

    @Test
    public void generateFactory_Constructor_Public_Protected() {

        String path = "Constructor_Public_Protected";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/UnderTest.java")
                );

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(path + "/expected/UnderTestMagnetFactory.java"));
    }

    @Test
    public void generateFactory_Constructor_Public_Private() {

        String path = "Constructor_Public_Private";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/UnderTest.java")
                );

        assertThat(compilation).succeededWithoutWarnings();
        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(path + "/expected/UnderTestMagnetFactory.java"));
    }

    @Test
    public void generateFactory_Constructor_PackagePrivate_PackagePrivate() {

        String path = "Constructor_PackagePrivate_PackagePrivate";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/UnderTest.java")
                );

        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("must have exactly one public or package-private constructor");
    }

    @Test
    public void generateFactory_Constructor_Private() {

        String path = "Constructor_Private";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/UnderTest.java")
                );

        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("must have exactly one public or package-private constructor");
    }

    @Test
    public void generateFactory_Constructor_Protected() {

        String path = "Constructor_Protected";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/UnderTest.java")
                );

        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("must have exactly one public or package-private constructor");
    }

    @Test
    public void generateFactory_Constructor_PackagePrivate_Private() {

        String path = "Constructor_PackagePrivate_Private";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/UnderTest.java")
                );

        assertThat(compilation).succeededWithoutWarnings();

        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(path + "/expected/UnderTestMagnetFactory.java"));
    }

//    @Test
    //TODO 不支持kotlin
    public void generateFactory_Constructor_Public_Public_Kotlin() {

        String path = "Constructor_Public_Public_Kotlin";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/UnderTest.java")
                );

        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("must have exactly one public or package-private constructor");
    }

    @Test
    public void generateFactory_Generics_GetSingle_Unchecked() {
        String path = "Generics_GetSingle_Unchecked";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/Dependency.java"),
                        withResource(path + "/UnderTest.java")
                );

        assertThat(compilation).succeededWithoutWarnings();

        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(path + "/expected/UnderTestMagnetFactory.java"));
    }

    @Test
    public void generateFactory_Generics_GetOptional_Unchecked() {
        String path = "Generics_GetOptional_Unchecked";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path + "/Dependency.java"),
                        withResource(path + "/UnderTest.java")
                );

        assertThat(compilation).succeededWithoutWarnings();

        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(path + "/expected/UnderTestMagnetFactory.java"));
    }

    @Test
    public void generateFactory_TypeAutoDetect_ExtendsObjectNoInterfaces() {
        String path = "TypeAutoDetect_ExtendsObjectNoInterfaces";
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        withResource(path+"/UnderTest.java")
                );

        assertThat(compilation).succeededWithoutWarnings();

        assertThat(compilation)
                .generatedSourceFile("app/UnderTestMagnetFactory")
                .hasSourceEquivalentTo(withResource(path+"/expected/UnderTestMagnetFactory.java"));
    }
}
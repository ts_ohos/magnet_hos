package magnet.processor;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.CompilationSubject;
import com.google.testing.compile.Compiler;
import com.google.testing.compile.JavaFileObjects;
import org.junit.Test;

import javax.annotation.processing.Processor;
import javax.tools.JavaFileObject;

public final class GenerateRegistryForInstanceFactoriesTest {
    private final JavaFileObject withResource(String name) {
        JavaFileObject javaFileObject = JavaFileObjects.forResource(this.getClass().getSimpleName() + '/' + name);
        return javaFileObject;
    }

    @Test
    public final void No_factories/* $FF was: No factories*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("App.java")});
        CompilationSubject.assertThat(compilation).succeeded();
        CompilationSubject.assertThat(compilation).generatedSourceFile("magnet/internal/MagnetIndexer").hasSourceEquivalentTo(this.withResource("expected/MagnetIndexer2.java"));
    }

    @Test
    public final void Single_factory/* $FF was: Single factory*/() {
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor())
                .compile(
                        this.withResource("App.java"),
                        this.withResource("Interface1.java"),
                        this.withResource("Implementation1.java")
                );

        CompilationSubject.assertThat(compilation).succeeded();

        CompilationSubject.assertThat(compilation)
                .generatedSourceFile("magnet/internal/MagnetIndexer")
                .hasSourceEquivalentTo(withResource("expected/MagnetIndexer1.java"));
    }

    @Test
    public final void Single_factory__inner_implementation/* $FF was: Single factory, inner implementation*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("App.java"), this.withResource("Interface7.java")});
        CompilationSubject.assertThat(compilation).succeeded();
        CompilationSubject.assertThat(compilation).generatedSourceFile("magnet/internal/MagnetIndexer").hasSourceEquivalentTo(this.withResource("expected/MagnetIndexer7.java"));
    }

    @Test
    public final void Many_factories__same_type/* $FF was: Many factories, same type*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("App.java"), this.withResource("Interface3.java"), this.withResource("Implementation3_1.java"), this.withResource("Implementation3_2.java")});
        CompilationSubject.assertThat(compilation).succeeded();
        CompilationSubject.assertThat(compilation).generatedSourceFile("magnet/internal/MagnetIndexer").hasSourceEquivalentTo(this.withResource("expected/MagnetIndexer3.java"));
    }

    @Test
    public final void Many_factories__same_type__same_classifier/* $FF was: Many factories, same type, same classifier*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("App.java"), this.withResource("Interface5.java"), this.withResource("Implementation5_1.java"), this.withResource("Implementation5_2.java")});
        CompilationSubject.assertThat(compilation).succeeded();
        CompilationSubject.assertThat(compilation).generatedSourceFile("magnet/internal/MagnetIndexer").hasSourceEquivalentTo(this.withResource("expected/MagnetIndexer5.java"));
    }

    @Test
    public final void Many_factories_same_type_different_classifiers/* $FF was: Many factories, same type, different classifiers*/() {
        Compilation compilation = Compiler.javac().
                withProcessors(new MagnetProcessor())
                .compile(
                        this.withResource("App.java"),
                        this.withResource("Interface4.java"),
                        this.withResource("Implementation4_1.java"),
                        this.withResource("Implementation4_2.java")
                );

        CompilationSubject.assertThat(compilation).succeeded();

        CompilationSubject.assertThat(compilation)
                .generatedSourceFile("magnet/internal/MagnetIndexer")
                .hasSourceEquivalentTo(this.withResource("expected/MagnetIndexer4.java"));
    }

    @Test
    public final void Many_factories__different_types/* $FF was: Many factories, different types*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("App.java"), this.withResource("Interface6_1.java"), this.withResource("Interface6_2.java"), this.withResource("Implementation6_1.java"), this.withResource("Implementation6_2.java")});
        CompilationSubject.assertThat(compilation).succeeded();
        CompilationSubject.assertThat(compilation).generatedSourceFile("magnet/internal/MagnetIndexer").hasSourceEquivalentTo(this.withResource("expected/MagnetIndexer6.java"));
    }
}
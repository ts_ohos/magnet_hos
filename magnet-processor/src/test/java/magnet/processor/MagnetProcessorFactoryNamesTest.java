package magnet.processor;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.CompilationSubject;
import com.google.testing.compile.Compiler;
import com.google.testing.compile.JavaFileObjects;
import org.junit.Test;

import javax.annotation.processing.Processor;
import javax.tools.JavaFileObject;

public class MagnetProcessorFactoryNamesTest {
    private JavaFileObject withResource(String name) {
        return JavaFileObjects.forResource(this.getClass().getSimpleName() + '/' + name);
    }

    @Test
    public void Generate_fully_named_factories_for_top_level_classes/* $FF was: Generate fully named factories for top level classes*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor)(new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Delegate1.java")});
        CompilationSubject.assertThat(compilation).succeededWithoutWarnings();
        CompilationSubject.assertThat(compilation).generatedSourceFile("test/Delegate1MagnetFactory").hasSourceEquivalentTo(this.withResource("generated/Delegate1MagnetFactory.java"));
    }

    @Test
    public void Generate_fully_named_factories_for_inner_classes/* $FF was: Generate fully named factories for inner classes*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor)(new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface1.java")});
        CompilationSubject.assertThat(compilation).succeededWithoutWarnings();
        CompilationSubject.assertThat(compilation).generatedSourceFile("test/Interface1DelegateMagnetFactory").hasSourceEquivalentTo(this.withResource("generated/Interface1DelegateMagnetFactory.java"));
    }
}

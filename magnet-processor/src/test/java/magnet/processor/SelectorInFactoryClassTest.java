package magnet.processor;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.CompilationSubject;
import com.google.testing.compile.Compiler;
import com.google.testing.compile.JavaFileObjects;
import org.junit.Test;

import javax.annotation.processing.Processor;
import javax.tools.JavaFileObject;

public class SelectorInFactoryClassTest {
    @Test
    public void Empty_selector_is_allowed/* $FF was: Empty selector is allowed*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface.java"), this.withResource("Implementation1.java")});
        CompilationSubject.assertThat(compilation).succeededWithoutWarnings();
        CompilationSubject.assertThat(compilation).generatedSourceFile("selector/Implementation1MagnetFactory").hasSourceEquivalentTo(this.withResource("generated/Implementation1MagnetFactory.java"));
    }

    @Test
    public void Invalid_selector_fails_compilation/* $FF was: Invalid selector fails compilation*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface.java"), this.withResource("Implementation2.java")});
        CompilationSubject.assertThat(compilation).failed();
        CompilationSubject.assertThat(compilation).hadErrorContaining("Invalid selector");
    }

    @Test
    public void Empty_selector_id_fails_compilation/* $FF was: Empty selector id fails compilation*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface.java"), this.withResource("Implementation3.java")});
        CompilationSubject.assertThat(compilation).failed();
        CompilationSubject.assertThat(compilation).hadErrorContaining("Invalid selector");
    }

    @Test
    public void Empty_selector_filed_fails_compilation/* $FF was: Empty selector filed fails compilation*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface.java"), this.withResource("Implementation4.java")});
        CompilationSubject.assertThat(compilation).failed();
        CompilationSubject.assertThat(compilation).hadErrorContaining("Invalid selector");
    }

    @Test
    public void Invalid_selector_operator_fails_compilation/* $FF was: Invalid selector operator fails compilation*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface.java"), this.withResource("Implementation5.java")});
        CompilationSubject.assertThat(compilation).failed();
        CompilationSubject.assertThat(compilation).hadErrorContaining("Invalid selector");
    }

    @Test
    public void Empty_selector_operand_fails_compilation/* $FF was: Empty selector operand fails compilation*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface.java"), this.withResource("Implementation6.java")});
        CompilationSubject.assertThat(compilation).failed();
        CompilationSubject.assertThat(compilation).hadErrorContaining("Invalid selector");
    }

    @Test
    public void Valid_selector__1_operand_/* $FF was: Valid selector (1 operand)*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface.java"), this.withResource("Implementation7.java")});
        CompilationSubject.assertThat(compilation).succeededWithoutWarnings();
        CompilationSubject.assertThat(compilation).generatedSourceFile("selector/Implementation7MagnetFactory").hasSourceEquivalentTo(this.withResource("generated/Implementation7MagnetFactory.java"));
    }

    @Test
    public void Valid_selector__2_operands__in_/* $FF was: Valid selector (2 operands, in)*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface.java"), this.withResource("Implementation8.java")});
        CompilationSubject.assertThat(compilation).succeededWithoutWarnings();
        CompilationSubject.assertThat(compilation).generatedSourceFile("selector/Implementation8MagnetFactory").hasSourceEquivalentTo(this.withResource("generated/Implementation8MagnetFactory.java"));
    }

    @Test
    public void Valid_selector__1_operand_____/* $FF was: Valid selector (1 operand, !=)*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface.java"), this.withResource("Implementation9.java")});
        CompilationSubject.assertThat(compilation).succeededWithoutWarnings();
        CompilationSubject.assertThat(compilation).generatedSourceFile("selector/Implementation9MagnetFactory").hasSourceEquivalentTo(this.withResource("generated/Implementation9MagnetFactory.java"));
    }

    @Test
    public void Valid_selector__2_operands___in_/* $FF was: Valid selector (2 operands, !in)*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface.java"), this.withResource("Implementation10.java")});
        CompilationSubject.assertThat(compilation).succeededWithoutWarnings();
        CompilationSubject.assertThat(compilation).generatedSourceFile("selector/Implementation10MagnetFactory").hasSourceEquivalentTo(this.withResource("generated/Implementation10MagnetFactory.java"));
    }

    private JavaFileObject withResource(String name) {
        return JavaFileObjects.forResource(this.getClass().getSimpleName() + '/' + name);
    }
}

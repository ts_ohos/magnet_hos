package magnet.processor;

import com.google.common.truth.Truth;
import com.squareup.javapoet.ClassName;
import magnet.processor.registry.instances.Indexer;
import magnet.processor.registry.instances.Model.Index;
import magnet.processor.registry.instances.Model.Inst;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class IndexerTest {
    @Test
    public final void test_IndexNodes() {
        Index index = new Indexer().index(unsortedNodes());
        Truth.assertThat(index.getInstances().toArray().equals(sortedNodes().toArray()));
    }

    @Test
    public final void test_IndexSections() {
        Index index = new Indexer().index(unsortedNodes());
        Truth.assertThat(index.getSections().size()).isEqualTo(3);
        Truth.assertThat(index.getSections().get(0).getType()).isEqualTo("AType");
        Truth.assertThat(index.getSections().get(0).getRanges().size()).isEqualTo(3);

        Truth.assertThat(index.getSections().get(1).getType()).isEqualTo("BType");
        Truth.assertThat(index.getSections().get(1).getRanges().size()).isEqualTo(1);

        Truth.assertThat(index.getSections().get(2).getType()).isEqualTo("CType");
        Truth.assertThat(index.getSections().get(2).getRanges().size()).isEqualTo(4);
    }

    private final List<Inst> unsortedNodes() {
        ClassName factory = ClassName.bestGuess("Factory");
        return new ArrayList<>(Arrays.asList(
                new Inst("AType", "", factory),
                new Inst("AType", "", factory),
                new Inst("AType", "one", factory),
                new Inst("AType", "one", factory),
                new Inst("AType", "two", factory),

                new Inst("BType", "", factory),
                new Inst("BType", "", factory),
                new Inst("BType", "", factory),

                new Inst("CType", "four", factory),
                new Inst("CType", "four", factory),
                new Inst("CType", "one", factory),
                new Inst("CType", "three", factory),
                new Inst("CType", "two", factory)));

    }

    private final List<Inst> sortedNodes() {
        ClassName factory = ClassName.bestGuess("Factory");
        return new ArrayList<>(Arrays.asList(new Inst("AType", "", factory),
                new Inst("AType", "", factory),
                new Inst("AType", "one", factory),
                new Inst("AType", "one", factory),
                new Inst("AType", "two", factory),
                new Inst("BType", "", factory),
                new Inst("BType", "", factory),
                new Inst("BType", "", factory),
                new Inst("CType", "four", factory),
                new Inst("CType", "four", factory),
                new Inst("CType", "one", factory),
                new Inst("CType", "three", factory),
                new Inst("CType", "two", factory)));
    }
}

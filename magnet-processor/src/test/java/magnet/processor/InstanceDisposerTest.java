package magnet.processor;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.CompilationSubject;
import com.google.testing.compile.Compiler;
import com.google.testing.compile.JavaFileObjects;

import javax.annotation.processing.Processor;
import javax.tools.JavaFileObject;

import org.junit.Test;

public final class InstanceDisposerTest {
    private final JavaFileObject withResource(String name) {
        JavaFileObject javaFileObject = JavaFileObjects.forResource(this.getClass().getSimpleName() + '/' + name);
        return javaFileObject;
    }

    @Test
    public final void Disposer_method_gets_generated/* $FF was: Disposer method gets generated*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface.java"), this.withResource("Implementation1.java")});
        CompilationSubject.assertThat(compilation).succeeded();
        CompilationSubject.assertThat(compilation).generatedSourceFile("test/Implementation1MagnetFactory").hasSourceEquivalentTo(this.withResource("expected/Implementation1MagnetFactory.java"));
    }

    @Test
    public final void Disposer_must_be_present/* $FF was: Disposer must be present*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface.java"), this.withResource("Implementation2.java")});
        CompilationSubject.assertThat(compilation).failed();
        CompilationSubject.assertThat(compilation).hadErrorContaining("disposer method");
    }

    @Test
    public final void Disposer_must_have_no_parameters/* $FF was: Disposer must have no parameters*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface.java"), this.withResource("Implementation3.java")});
        CompilationSubject.assertThat(compilation).failed();
        CompilationSubject.assertThat(compilation).hadErrorContaining("must have no parameters");
    }

    @Test
    public final void Disposer_must_return_void/* $FF was: Disposer must return void*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface.java"), this.withResource("Implementation4.java")});
        CompilationSubject.assertThat(compilation).failed();
        CompilationSubject.assertThat(compilation).hadErrorContaining("must return void");
    }

    @Test
    public final void Disposer_cannot_be_used_with_UNSCOPED_instances/* $FF was: Disposer cannot be used with UNSCOPED instances*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface.java"), this.withResource("Implementation5.java")});
        CompilationSubject.assertThat(compilation).failed();
        CompilationSubject.assertThat(compilation).hadErrorContaining("UNSCOPED");
    }
}

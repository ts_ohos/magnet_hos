package magnet.processor;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.CompilationSubject;
import com.google.testing.compile.Compiler;
import com.google.testing.compile.JavaFileObjects;
import org.junit.Test;

import javax.annotation.processing.Processor;
import javax.tools.JavaFileObject;

public class SiblingTypesTest {
    @Test
    public void Either_type___or_types___is_required/* $FF was: Either type() or types() is required*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor)(new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface1.java"), this.withResource("Interface2.java"), this.withResource("Implementation1.java")});
        CompilationSubject.assertThat(compilation).failed();
        CompilationSubject.assertThat(compilation).hadErrorContaining("must declare");
    }

    @Test
    public void Both_type___or_types___are_not_allowed/* $FF was: Both type() or types() are not allowed*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor)(new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface1.java"), this.withResource("Interface2.java"), this.withResource("Implementation2.java")});
        CompilationSubject.assertThat(compilation).failed();
        CompilationSubject.assertThat(compilation).hadErrorContaining("not both");
    }

    @Test
    public void Inheritance_verification_fails_for_types__/* $FF was: Inheritance verification fails for types()*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor)(new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface1.java"), this.withResource("Interface2.java"), this.withResource("Implementation3.java")});
        CompilationSubject.assertThat(compilation).failed();
        CompilationSubject.assertThat(compilation).hadErrorContaining("must implement siblings.Interface2");
    }

    @Test
    public void types___generates_multiple_factories/* $FF was: types() generates multiple factories*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor)(new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface1.java"), this.withResource("Interface2.java"), this.withResource("Implementation4.java")});
        CompilationSubject.assertThat(compilation).succeededWithoutWarnings();
        CompilationSubject.assertThat(compilation).generatedSourceFile("siblings/Implementation4Interface1MagnetFactory").hasSourceEquivalentTo(this.withResource("generated/Implementation4Interface1MagnetFactory.java"));
        CompilationSubject.assertThat(compilation).generatedSourceFile("siblings/Implementation4Interface2MagnetFactory").hasSourceEquivalentTo(this.withResource("generated/Implementation4Interface2MagnetFactory.java"));
    }

    @Test
    public void types___must_be_used_with_scoped_instances/* $FF was: types() must be used with scoped instances*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor)(new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface1.java"), this.withResource("Interface2.java"), this.withResource("Implementation5.java")});
        CompilationSubject.assertThat(compilation).failed();
        CompilationSubject.assertThat(compilation).hadErrorContaining("must be used with scoped instances");
    }

    private JavaFileObject withResource(String name) {
        return JavaFileObjects.forResource(this.getClass().getSimpleName() + '/' + name);
    }
}

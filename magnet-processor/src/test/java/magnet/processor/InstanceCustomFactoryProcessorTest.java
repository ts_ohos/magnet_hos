package magnet.processor;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.CompilationSubject;
import com.google.testing.compile.Compiler;
import com.google.testing.compile.JavaFileObjects;

import javax.annotation.processing.Processor;
import javax.tools.JavaFileObject;

import org.junit.Test;

public final class InstanceCustomFactoryProcessorTest {
    private final JavaFileObject withResource(String name) {
        JavaFileObject javaFileObject = JavaFileObjects.forResource(this.getClass().getSimpleName() + '/' + name);
        return javaFileObject;
    }

    @Test
    public final void Not_parametrized_custom_Factory/* $FF was: Not parametrized custom Factory*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface1.java"), this.withResource("Implementation1.java"), this.withResource("CustomFactory1.java")});
        CompilationSubject.assertThat(compilation).succeeded();
        CompilationSubject.assertThat(compilation).generatedSourceFile("test/Implementation1MagnetFactory").hasSourceEquivalentTo(this.withResource("expected/Implementation1MagnetFactory.java"));
    }

    @Test
    public final void Parametrized_custom_Factory/* $FF was: Parametrized custom Factory*/() {
        Compilation compilation = Compiler.javac().withProcessors(new Processor[]{(Processor) (new MagnetProcessor())}).compile(new JavaFileObject[]{this.withResource("Interface2.java"), this.withResource("Implementation2.java"), this.withResource("CustomFactory2.java")});
        CompilationSubject.assertThat(compilation).succeeded();
        CompilationSubject.assertThat(compilation).generatedSourceFile("test/Implementation2MagnetFactory").hasSourceEquivalentTo(this.withResource("expected/Implementation2MagnetFactory.java"));
    }

    @Test
    public final void Custom_Factory_for_implementation_with_dependencies/* $FF was: Custom Factory for implementation with dependencies*/() {
        Compilation compilation = Compiler.javac()
                .withProcessors(new MagnetProcessor()).
                        compile(
                                this.withResource("Interface3.java"),
                                this.withResource("Implementation3.java"),
                                this.withResource("CustomFactory3.java")
                        );
        CompilationSubject.assertThat(compilation).succeeded();

        CompilationSubject.assertThat(compilation)
                .generatedSourceFile("test/Implementation3MagnetFactory")
                .hasSourceEquivalentTo(this.withResource("expected/Implementation3MagnetFactory.java"));
    }
}

package magnet.processor.instances.parser;

import magnet.processor.MagnetProcessorEnv;
import magnet.processor.instances.aspects.disposer.DisposerValidator;
import magnet.processor.instances.aspects.limitedto.LimitedToValidator;
import magnet.processor.instances.aspects.type.TypeAndTypesValidator;

import javax.lang.model.element.Element;
import java.util.Arrays;
import java.util.List;

public interface AspectValidator {

    class Registry {

        private static volatile Registry registry;
        public List<AspectValidator> VALIDATORS;

        private Registry() {
            VALIDATORS = Arrays.asList(
                    TypeAndTypesValidator.INSTANCE(),
                    DisposerValidator.INSTANCE(),
                    LimitedToValidator.INSTANCE()
            );
        }

        public static Registry INSTANCE() {
            synchronized (Registry.class) {
                if (registry == null) {
                    synchronized (Registry.class) {
                        registry = new Registry();
                    }
                }
            }
            return registry;
        }
    }

    <E extends Element> ParserInstance<E> validate(ParserInstance<E> parserInstance, MagnetProcessorEnv env) throws Throwable;

}

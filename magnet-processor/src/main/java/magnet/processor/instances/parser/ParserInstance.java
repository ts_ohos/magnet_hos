package magnet.processor.instances.parser;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.TypeName;
import magnet.Classifier;
import magnet.Scoping;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.util.ArrayList;
import java.util.List;

public class ParserInstance<E extends Element> {

    private E element;
    private TypeElement declaredType = null;
    private List<TypeElement> declaredTypes = null;
    private List<ClassName> types = new ArrayList<>();
    private String classifier = Classifier.NONE;
    private String scoping = Scoping.TOPMOST.name();
    private String limitedTo ="";
    private List<String> selector = null;
    private TypeName factory = null;
    private String disposer = null;
    private boolean disabled=false;

    public E getElement() {
        return element;
    }

    public void setElement(E element) {
        this.element = element;
    }

    public TypeElement getDeclaredType() {
        return declaredType;
    }

    public void setDeclaredType(TypeElement declaredType) {
        this.declaredType = declaredType;
    }

    public List<TypeElement> getDeclaredTypes() {
        return declaredTypes;
    }

    public void setDeclaredTypes(List<TypeElement> declaredTypes) {
        this.declaredTypes = declaredTypes;
    }

    public List<ClassName> getTypes() {
        return types;
    }

    public void setTypes(List<ClassName> types) {
        this.types = types;
    }

    public String getClassifier() {
        return classifier;
    }

    public void setClassifier(String classifier) {
        this.classifier = classifier;
    }

    public String getScoping() {
        return scoping;
    }

    public void setScoping(String scoping) {
        this.scoping = scoping;
    }

    public String getLimitedTo() {
        return limitedTo;
    }

    public void setLimitedTo(String limitedTo) {
        this.limitedTo = limitedTo;
    }

    public List<String> getSelector() {
        return selector;
    }

    public void setSelector(List<String> selector) {
        this.selector = selector;
    }

    public TypeName getFactory() {
        return factory;
    }

    public void setFactory(TypeName factory) {
        this.factory = factory;
    }

    public String getDisposer() {
        return disposer;
    }

    public void setDisposer(String disposer) {
        this.disposer = disposer;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public ParserInstance copy() {
        ParserInstance<E> parserInstance = new ParserInstance<>();
        parserInstance.element= element;
        parserInstance.declaredType= declaredType;
        parserInstance.declaredTypes = declaredTypes;
        parserInstance.types = types;
        parserInstance.classifier = classifier;
        parserInstance.scoping = scoping;
        parserInstance.limitedTo =limitedTo;
        parserInstance.selector = selector;
        parserInstance.factory = factory;
        parserInstance.disposer = disposer;
        parserInstance.disabled = disabled;
        return parserInstance;
    }
}

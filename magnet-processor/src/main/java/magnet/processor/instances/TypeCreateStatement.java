package magnet.processor.instances;

import com.squareup.javapoet.ClassName;

public class TypeCreateStatement extends CreateStatement{

    ClassName instanceType;

    public TypeCreateStatement(ClassName instanceType) {
        this.instanceType = instanceType;
    }

    public ClassName getInstanceType() {
        return instanceType;
    }
}

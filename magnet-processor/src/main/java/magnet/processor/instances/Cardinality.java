package magnet.processor.instances;

public enum Cardinality {

    Single, Optional, Many

}

package magnet.processor.instances;

import com.squareup.javapoet.ClassName;

public class StaticMethodCreateStatement extends CreateStatement {
    ClassName staticMethodClassName;
    String staticMethodName;

    public StaticMethodCreateStatement(ClassName staticMethodClassName, String staticMethodName) {
        this.staticMethodClassName = staticMethodClassName;
        this.staticMethodName = staticMethodName;
    }

    public ClassName getStaticMethodClassName() {
        return staticMethodClassName;
    }

    public String getStaticMethodName() {
        return staticMethodName;
    }
}

package magnet.processor.instances;

import java.util.List;

public class GetSelectorMethod {
    private List<String> selectorArguments;

    public GetSelectorMethod(List<String> selectorArguments) {
        this.selectorArguments = selectorArguments;
    }

    public void accept(FactoryTypeVisitor visitor) {
        visitor.enterGetSelectorMethod(this);
        for (String argument : selectorArguments) {
            visitor.visitSelectorArgument(argument);
        }
        visitor.exitGetSelectorMethod(this);
    }

    public List<String> getSelectorArguments() {
        return selectorArguments;
    }
}

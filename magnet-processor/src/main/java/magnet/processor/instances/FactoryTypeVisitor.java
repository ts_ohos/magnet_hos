package magnet.processor.instances;

import com.squareup.javapoet.ClassName;

public interface FactoryTypeVisitor {

    default void enterFactoryClass(FactoryType factoryType) throws IllegalAccessException {};

    default void enterCreateMethod(CreateMethod createMethod){};

    default void visitCreateMethodParameter(MethodParameter parameter){};

    default void exitCreateMethod(CreateMethod createMethod){};

    default void visit(GetScopingMethod method){};

    default void visit(GetLimitMethod method){};

    default void enterSiblingTypesMethod(GetSiblingTypesMethod method){};

    default void visitSiblingType(ClassName type){};

    default void exitSiblingTypesMethod(GetSiblingTypesMethod method){};

    default void enterGetSelectorMethod(GetSelectorMethod method){};

    default void visitSelectorArgument(String argument){};

    default void exitGetSelectorMethod(GetSelectorMethod method){};

    default void exitFactoryClass(FactoryType factory){};
}

package magnet.processor.instances;

public class GetLimitMethod {
    String limit;

    public GetLimitMethod(String limit) {
        this.limit = limit;
    }

    public void accept(FactoryTypeVisitor visitor) {
        visitor.visit(this);
    }

    public String getLimit() {
        return limit;
    }
}

package magnet.processor.instances;

public class GetScopingMethod {
    private String scoping;

    public GetScopingMethod(String scoping) {
        this.scoping = scoping;
    }

    public void accept(FactoryTypeVisitor visitor) {
        visitor.visit(this);
    }

    public String getScoping() {
        return scoping;
    }
}

package magnet.processor.instances;

public class Expression {

    public static class Scope extends Expression {

        private static volatile Scope scope;

        private Scope() {

        }

        public static Scope INSTANCE() {
            synchronized (Scope.class) {
                if (scope == null) {
                    synchronized (Scope.class) {
                        scope = new Scope();
                    }
                }
            }
            return scope;
        }
    }

    public static class Getter extends Expression {
        public Cardinality cardinality;

        public Getter(Cardinality cardinality) {
            this.cardinality = cardinality;
        }
    }

    public static class LazyGetter {
        public Cardinality cardinality;

        public LazyGetter(Cardinality cardinality) {
            this.cardinality = cardinality;
        }
    }
}

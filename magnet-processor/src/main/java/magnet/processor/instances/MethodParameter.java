package magnet.processor.instances;

import com.squareup.javapoet.TypeName;

public class MethodParameter {

    String name;
    Expression expression;
    TypeName returnType;
    TypeName parameterType;
    String classifier;
    Boolean typeErased;

    public MethodParameter(String name, Expression expression, TypeName returnType, TypeName parameterType, String classifier, Boolean typeErased) {
        this.name = name;
        this.expression = expression;
        this.returnType = returnType;
        this.parameterType = parameterType;
        this.classifier = classifier;
        this.typeErased = typeErased;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public TypeName getReturnType() {
        return returnType;
    }

    public void setReturnType(TypeName returnType) {
        this.returnType = returnType;
    }

    public TypeName getParameterType() {
        return parameterType;
    }

    public void setParameterType(TypeName parameterType) {
        this.parameterType = parameterType;
    }

    public String getClassifier() {
        return classifier;
    }

    public void setClassifier(String classifier) {
        this.classifier = classifier;
    }

    public Boolean getTypeErased() {
        return typeErased;
    }

    public void setTypeErased(Boolean typeErased) {
        this.typeErased = typeErased;
    }

    public void accept(FactoryTypeVisitor visitor) {
        visitor.visitCreateMethodParameter(this);
    }
}

package magnet.processor.instances;

import com.squareup.javapoet.ClassName;

import java.util.List;

public class GetSiblingTypesMethod {
    List<ClassName> siblingTypes;

    public GetSiblingTypesMethod(List<ClassName> siblingTypes) {
        this.siblingTypes = siblingTypes;
    }

    public void accept(FactoryTypeVisitor visitor) {
        visitor.enterSiblingTypesMethod(this);
        for (ClassName siblingType : siblingTypes) {
            visitor.visitSiblingType(siblingType);
        }
        visitor.exitSiblingTypesMethod(this);
    }

    public List<ClassName> getSiblingTypes() {
        return siblingTypes;
    }
}

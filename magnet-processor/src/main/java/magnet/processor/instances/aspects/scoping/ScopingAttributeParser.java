package magnet.processor.instances.aspects.scoping;

import magnet.processor.instances.parser.AttributeParser;
import magnet.processor.instances.parser.ParserInstance;

import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;

public class ScopingAttributeParser extends AttributeParser {

    private static volatile ScopingAttributeParser parser;

    private ScopingAttributeParser() {
        super("scoping");
    }

    public static ScopingAttributeParser INSTANCE() {
        synchronized (ScopingAttributeParser.class) {
            if (parser == null) {
                synchronized (ScopingAttributeParser.class) {
                    parser = new ScopingAttributeParser();
                }
            }
        }
        return parser;
    }

    @Override
    public <E extends Element> ParserInstance<E> parse(Scope<E> scope, AnnotationValue value) {
        ParserInstance instance = scope.getInstance().copy();
        instance.setScoping(value.getValue().toString());
        return instance;
    }
}

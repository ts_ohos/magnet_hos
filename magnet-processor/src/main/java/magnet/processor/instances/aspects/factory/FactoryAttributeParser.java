package magnet.processor.instances.aspects.factory;

import com.squareup.javapoet.TypeName;
import magnet.processor.MagnetProcessorEnv;
import magnet.processor.instances.parser.AttributeParser;
import magnet.processor.instances.parser.ParserInstance;

import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;

public class FactoryAttributeParser extends AttributeParser {

    private static FactoryAttributeParser INSTANCE;

    public static FactoryAttributeParser INSTANCE() {
        synchronized (FactoryAttributeParser.class) {
            if (INSTANCE == null) {
                synchronized (FactoryAttributeParser.class) {
                    INSTANCE = new FactoryAttributeParser();
                }
            }
        }
        return INSTANCE;
    }

    private FactoryAttributeParser() {
        super("factory");
    }

    @Override

    public <E extends Element> ParserInstance<E> parse(Scope<E> scope, AnnotationValue value) {
        ParserInstance instance = scope.getInstance().copy();
        instance.setFactory(parseFactoryType(scope, value));
        return instance;
    }

    private <E extends Element> TypeName parseFactoryType(Scope<E> scope, AnnotationValue value) {
        MagnetProcessorEnv env = scope.getEnv();
        return TypeName.get(env.getAnnoation().getTypeElement(value).asType());
    }
}

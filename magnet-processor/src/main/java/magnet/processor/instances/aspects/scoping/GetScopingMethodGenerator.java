package magnet.processor.instances.aspects.scoping;

import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import magnet.Scoping;
import magnet.processor.instances.GetScopingMethod;
import magnet.processor.instances.generator.AspectGenerator;

import javax.lang.model.element.Modifier;

public class GetScopingMethodGenerator implements AspectGenerator {

    private MethodSpec getScoping = null;

    @Override
    public void generate(TypeSpec.Builder classBuilder) {
        if (getScoping != null) {
            classBuilder.addMethod(getScoping);
        }
    }

    @Override
    public void reset() {
        getScoping = null;
    }

    public void visit(GetScopingMethod method) {
        boolean defaultImplementationAvailable = method.getScoping().equals(Scoping.TOPMOST.name());
        if (!defaultImplementationAvailable) {
            getScoping = MethodSpec
                    .methodBuilder("getScoping")
                    .addModifiers(Modifier.PUBLIC)
                    .addAnnotation(Override.class)
                    .returns(Scoping.class)
                    .addStatement("return $T.$L", Scoping.class, method.getScoping())
                    .build();
        }
    }
}

package magnet.processor.instances.aspects.limitedto;

import magnet.Scoping;
import magnet.processor.MagnetProcessorEnv;
import magnet.processor.common.ValidationException;
import magnet.processor.instances.parser.AspectValidator;
import magnet.processor.instances.parser.ParserInstance;
import org.jetbrains.annotations.NotNull;

public class LimitedToValidator implements AspectValidator {

    private static volatile LimitedToValidator INSTANCE;

    public static LimitedToValidator INSTANCE() {
        synchronized (LimitedToValidator.class) {
            if (INSTANCE == null) {
                synchronized (LimitedToValidator.class) {
                    INSTANCE = new LimitedToValidator();
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public ParserInstance validate(@NotNull ParserInstance parserInstance, @NotNull MagnetProcessorEnv env) throws ValidationException {
        if ((parserInstance.getLimitedTo().equals("*"))) {
            throw new ValidationException(parserInstance.getElement(), "Limit must not use reserved '*' value. Use another value.");
        } else {
            CharSequence charSequence = (CharSequence) parserInstance.getLimitedTo();
            if (charSequence.length() > 0 && (parserInstance.getScoping().equals(Scoping.UNSCOPED.name()))) {
                throw new ValidationException(parserInstance.getElement(), "Limit can only be used with Scoping.TOPMOST and Scoping.DIRECT." + " Current scoping: Scoping." + parserInstance.getScoping());
            } else {
                return parserInstance;
            }
        }
    }
}

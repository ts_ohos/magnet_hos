package magnet.processor.instances.aspects.limitedto;

import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import magnet.processor.instances.GetLimitMethod;
import magnet.processor.instances.generator.AspectGenerator;

import javax.lang.model.element.Modifier;
import java.lang.reflect.Type;

public class GetLimitMethodGenerator implements AspectGenerator {

    private MethodSpec getLimit;

    public void reset() {
        getLimit = null;
    }

    public void generate(TypeSpec.Builder classBuilder) {
        MethodSpec methodSpec = this.getLimit;
        if (methodSpec != null) {
            MethodSpec spec = methodSpec;
            classBuilder.addMethod(spec);
        }
    }

    public void visit(GetLimitMethod method) {
        this.getLimit = MethodSpec
                .methodBuilder("getLimit")
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(Override.class)
                .returns((Type) String.class)
                .addStatement("return $S", new Object[]{method.getLimit()})
                .build();

    }
}

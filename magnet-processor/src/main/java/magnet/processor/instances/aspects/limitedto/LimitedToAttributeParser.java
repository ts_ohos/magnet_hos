package magnet.processor.instances.aspects.limitedto;

import magnet.processor.common.ValidationException;
import magnet.processor.instances.parser.AttributeParser;
import magnet.processor.instances.parser.ParserInstance;

import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;

public class LimitedToAttributeParser extends AttributeParser {

    private static volatile LimitedToAttributeParser INSTANCE;

    public static LimitedToAttributeParser INSTANCE() {
        synchronized (LimitedToAttributeParser.class) {
            if (INSTANCE == null) {
                synchronized (LimitedToAttributeParser.class) {
                    INSTANCE = new LimitedToAttributeParser();
                }
            }
        }
        return INSTANCE;
    }

    public LimitedToAttributeParser() {
        super("limitedTo");
    }

    @Override
    public <E extends Element> ParserInstance<E> parse(Scope<E> scope, AnnotationValue value) throws ValidationException {
        ParserInstance copy = scope.getInstance().copy();
        copy.setLimitedTo(value.getValue().toString());
        return copy;
    }
}

package magnet.processor.instances.aspects.disposer;

import magnet.Scoping;
import magnet.processor.MagnetProcessorEnv;
import magnet.processor.common.ValidationException;
import magnet.processor.instances.parser.AspectValidator;
import magnet.processor.instances.parser.ParserInstance;

import javax.lang.model.element.Element;


public class DisposerValidator implements AspectValidator{

    private static DisposerValidator INSTANCE;

    private DisposerValidator(){}

    public static DisposerValidator INSTANCE(){
        synchronized (DisposerValidator.class){
            if (INSTANCE == null){
                INSTANCE = new DisposerValidator();
            }
        }
        return INSTANCE;
    }

    @Override
    public <E extends Element> ParserInstance<E> validate(ParserInstance<E> parserInstance, MagnetProcessorEnv env) throws ValidationException {
        if (parserInstance.getDisposer() != null && parserInstance.getScoping().equals(Scoping.UNSCOPED.name())){
            throw new ValidationException(parserInstance.getElement(),"Disposer cannot be used with UNSCOPED instances.");
        }
        return parserInstance;
    }
}


package magnet.processor.instances.aspects.classifier;

import magnet.processor.instances.parser.AttributeParser;
import magnet.processor.instances.parser.ParserInstance;

import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;

public class ClassifierAttributeParser extends AttributeParser {
    private static ClassifierAttributeParser INSTANCE;

    private ClassifierAttributeParser() {
        super("classifier");
    }

    public static ClassifierAttributeParser INSTANCE(){
        synchronized (ClassifierAttributeParser.class){
            if (INSTANCE == null){
                    INSTANCE = new ClassifierAttributeParser();
                }
        }
        return INSTANCE;
    }

    @Override
    public <E extends Element> ParserInstance<E> parse(Scope<E> scope, AnnotationValue value) {
        ParserInstance copy = scope.getInstance().copy();
        copy.setClassifier(value.getValue().toString());
        return copy;
    }
}

package magnet.processor.instances.aspects.factory;

import com.squareup.javapoet.TypeSpec;
import magnet.processor.instances.CreateMethod;
import magnet.processor.instances.FactoryType;
import magnet.processor.instances.MethodParameter;

public class DefaultCreateMethodGenerator implements CreateMethodGenerator {

    private CustomFactoryCreateMethodGenerator customFactoryGenerator = new CustomFactoryCreateMethodGenerator();
    private StandardFactoryCreateMethodGenerator standardFactoryGenerator = new StandardFactoryCreateMethodGenerator();

    private CreateMethodGenerator impl;

    @Override
    public void visitFactoryClass(FactoryType factoryType) throws IllegalAccessException {
        if (factoryType.getCustomFactoryType() != null) {
            impl = customFactoryGenerator;
        } else {
            impl = standardFactoryGenerator;
        }
        impl.visitFactoryClass(factoryType);
    }

    @Override
    public void enterCreateMethod(CreateMethod createMethod) {
        impl.enterCreateMethod(createMethod);
    }

    @Override
    public void visitCreateMethodParameter(MethodParameter parameter) {
        impl.visitCreateMethodParameter(parameter);
    }

    @Override
    public void exitCreateMethod() {
        impl.exitCreateMethod();
    }

    @Override
    public void generate(TypeSpec.Builder typeBuilder) {
        impl.generate(typeBuilder);
    }
}

package magnet.processor.instances.aspects.disabled;

import magnet.processor.instances.parser.AttributeParser;
import magnet.processor.instances.parser.ParserInstance;

import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;

public class DisabledAttributeParser extends AttributeParser{

    private static DisabledAttributeParser INSTANCE;

    private DisabledAttributeParser() {
        super("disabled");
    }

    public static DisabledAttributeParser INSTANCE(){
        synchronized (DisabledAttributeParser.class){
            if (INSTANCE == null){
                    INSTANCE = new DisabledAttributeParser();
            }
        }
        return INSTANCE;
    }

    @Override
    public <E extends Element> ParserInstance<E> parse(Scope<E> scope, AnnotationValue value) {
        ParserInstance copy = scope.getInstance().copy();
        copy.setDisabled(Boolean.parseBoolean(value.getValue().toString()));
        return copy;
    }
}

package magnet.processor.instances.aspects.disposer;

import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import magnet.processor.instances.FactoryType;

import javax.lang.model.element.Modifier;

public class IsDisposableMethodGenerator {

    private MethodSpec.Builder methodBuilder = null;

    public void visitFactoryClass(FactoryType factoryType) {
        if (factoryType == null) return;
        methodBuilder =
                factoryType.getDisposerMethodName() == null ?
                        null :
                        MethodSpec.methodBuilder("isDisposable")
                                .addAnnotation(Override.class)
                                .addModifiers(Modifier.PUBLIC)
                                .returns(TypeName.BOOLEAN)
                                .addStatement("return true");
    }

    public void generate(TypeSpec.Builder typeBuilder) {
        if (methodBuilder == null) return;
        typeBuilder.addMethod(methodBuilder.build());
    }
}

package magnet.processor.instances;

import java.util.List;
import java.util.function.Consumer;

public class CreateMethod {
    List<MethodParameter> methodParameter;

    public CreateMethod(List<MethodParameter> methodParameter) {
        this.methodParameter = methodParameter;
    }

    public void accept(FactoryTypeVisitor visitor) {
        visitor.enterCreateMethod(this);
        methodParameter.forEach(new Consumer<MethodParameter>() {
            @Override
            public void accept(MethodParameter methodParameter) {
                methodParameter.accept(visitor);
            }
        });
        visitor.exitCreateMethod(this);
    }
}

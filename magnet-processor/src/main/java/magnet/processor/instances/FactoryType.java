package magnet.processor.instances;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.TypeName;

import javax.lang.model.element.Element;

public class FactoryType {
    Element element;
    ClassName interfaceType;
    String classifier;
    String scoping;
    boolean disabled;
    ClassName factoryType;
    ClassName implementationType;
    TypeName customFactoryType;
    String disposerMethodName;
    CreateStatement createStatement;
    CreateMethod createMethod;
    GetScopingMethod getScopingMethod;
    GetLimitMethod getLimitMethod;
    GetSelectorMethod getSelectorMethod;
    GetSiblingTypesMethod getSiblingTypesMethod;

    public FactoryType(Element element,
                       ClassName interfaceType,
                       String classifier,
                       String scoping,
                       boolean disabled,
                       ClassName factoryType,
                       ClassName implementationType,
                       TypeName customFactoryType,
                       String disposerMethodName,
                       CreateStatement createStatement,
                       CreateMethod createMethod,
                       GetScopingMethod getScopingMethod,
                       GetLimitMethod getLimitMethod,
                       GetSelectorMethod getSelectorMethod,
                       GetSiblingTypesMethod getSiblingTypesMethod) {
        this.element = element;
        this.interfaceType = interfaceType;
        this.classifier = classifier;
        this.scoping = scoping;
        this.disabled = disabled;
        this.factoryType = factoryType;
        this.implementationType = implementationType;
        this.customFactoryType = customFactoryType;
        this.disposerMethodName = disposerMethodName;
        this.createStatement = createStatement;
        this.createMethod = createMethod;
        this.getScopingMethod = getScopingMethod;
        this.getLimitMethod = getLimitMethod;
        this.getSelectorMethod = getSelectorMethod;
        this.getSiblingTypesMethod = getSiblingTypesMethod;
    }

    public void accept(FactoryTypeVisitor visitor) throws IllegalAccessException {
        visitor.enterFactoryClass(this);
        createMethod.accept(visitor);
        getScopingMethod.accept(visitor);
        if (getLimitMethod != null) {
            getLimitMethod.accept(visitor);
        }
        if (getSiblingTypesMethod != null) {
            getSiblingTypesMethod.accept(visitor);
        }
        if (getSelectorMethod != null) {
            getSelectorMethod.accept(visitor);
        }
        visitor.exitFactoryClass(this);
    }

    public Element getElement() {
        return element;
    }

    public void setElement(Element element) {
        this.element = element;
    }

    public ClassName getInterfaceType() {
        return interfaceType;
    }

    public void setInterfaceType(ClassName interfaceType) {
        this.interfaceType = interfaceType;
    }

    public String getClassifier() {
        return classifier;
    }

    public void setClassifier(String classifier) {
        this.classifier = classifier;
    }

    public String getScoping() {
        return scoping;
    }

    public void setScoping(String scoping) {
        this.scoping = scoping;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public ClassName getFactoryType() {
        return factoryType;
    }

    public void setFactoryType(ClassName factoryType) {
        this.factoryType = factoryType;
    }

    public ClassName getImplementationType() {
        return implementationType;
    }

    public void setImplementationType(ClassName implementationType) {
        this.implementationType = implementationType;
    }

    public TypeName getCustomFactoryType() {
        return customFactoryType;
    }

    public void setCustomFactoryType(TypeName customFactoryType) {
        this.customFactoryType = customFactoryType;
    }

    public String getDisposerMethodName() {
        return disposerMethodName;
    }

    public void setDisposerMethodName(String disposerMethodName) {
        this.disposerMethodName = disposerMethodName;
    }

    public CreateStatement getCreateStatement() {
        return createStatement;
    }

    public void setCreateStatement(CreateStatement createStatement) {
        this.createStatement = createStatement;
    }

    public CreateMethod getCreateMethod() {
        return createMethod;
    }

    public void setCreateMethod(CreateMethod createMethod) {
        this.createMethod = createMethod;
    }

    public GetScopingMethod getGetScopingMethod() {
        return getScopingMethod;
    }

    public void setGetScopingMethod(GetScopingMethod getScopingMethod) {
        this.getScopingMethod = getScopingMethod;
    }

    public GetLimitMethod getGetLimitMethod() {
        return getLimitMethod;
    }

    public void setGetLimitMethod(GetLimitMethod getLimitMethod) {
        this.getLimitMethod = getLimitMethod;
    }

    public GetSelectorMethod getGetSelectorMethod() {
        return getSelectorMethod;
    }

    public void setGetSelectorMethod(GetSelectorMethod getSelectorMethod) {
        this.getSelectorMethod = getSelectorMethod;
    }

    public GetSiblingTypesMethod getGetSiblingTypesMethod() {
        return getSiblingTypesMethod;
    }

    public void setGetSiblingTypesMethod(GetSiblingTypesMethod getSiblingTypesMethod) {
        this.getSiblingTypesMethod = getSiblingTypesMethod;
    }
}

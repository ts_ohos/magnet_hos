package magnet.processor.instances.generator;

import com.squareup.javapoet.TypeSpec;

public interface AspectGenerator {

    void generate(TypeSpec.Builder classBuilder);

    void reset();
}

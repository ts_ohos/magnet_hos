package magnet.processor.instances.generator;

import com.squareup.javapoet.TypeSpec;

public class Aspect<G extends AspectGenerator> {

    private boolean visited = false;
    private G generator;

    public Aspect(G generator) {
        this.generator = generator;
    }

    public interface VisitBlock<G extends AspectGenerator> {
        void block(G generator);
    }

    public void visit(VisitBlock block) {
        block.block(generator);
        visited = true;
    }

    public void generate(TypeSpec.Builder classBuilder) {
        if (visited) {
            generator.generate(classBuilder);
        }
        generator.reset();
        visited = false;
    }
}

package magnet.processor.instances.generator;

import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.TypeSpec;

import javax.annotation.processing.Filer;
import java.io.IOException;

public class CodeWriter {

    private String filePackage;
    private TypeSpec fileTypeSpec;

    public CodeWriter(String filePackage, TypeSpec fileTypeSpec) {
        this.filePackage = filePackage;
        this.fileTypeSpec = fileTypeSpec;
    }

    public void writeInto(Filer filer) {
        try {
            JavaFile
                    .builder(filePackage, fileTypeSpec)
                    .skipJavaLangImports(true)
                    .build()
                    .writeTo(filer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package magnet.processor.instances.generator;

import magnet.processor.instances.FactoryType;

public interface CodeGenerator {

    CodeWriter generateFrom(FactoryType factoryType) throws IllegalAccessException;
}

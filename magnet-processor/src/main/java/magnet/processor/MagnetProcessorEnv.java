package magnet.processor;

import magnet.processor.common.AnnotationValueExtractor;
import magnet.processor.common.CompilationException;
import magnet.processor.common.ValidationException;

import javax.annotation.processing.Filer;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;

public class MagnetProcessorEnv {

    private ProcessingEnvironment processEnvironment;
    private AnnotationValueExtractor annoation;

    public Filer getFiler() {
        return processEnvironment.getFiler();
    }

    public Elements getElements() {
        return processEnvironment.getElementUtils();
    }

    public Types getTypes() {
        return processEnvironment.getTypeUtils();
    }

    public AnnotationValueExtractor getAnnoation() {
        return annoation;
    }

    public MagnetProcessorEnv(ProcessingEnvironment processEnvironment) {
        this.processEnvironment = processEnvironment;
        this.annoation = new AnnotationValueExtractor(getElements());
    }

    public void reportError(ValidationException e) {
        processEnvironment
                .getMessager()
                .printMessage(Diagnostic.Kind.ERROR, e.getMessage(), e.getElement());
    }

    public void reportError(CompilationException e) {

        String msg = e.getMessage() == null ? "none" : e.getMessage();

        processEnvironment.getMessager().printMessage(
                Diagnostic.Kind.ERROR,
                "Unexpected compilation error," +
                        " please file the bug at https://github.com/beworker/magnet/issues." +
                        " Message: " + msg + ".",
                e.getElement()
        );
    }

    public void reportError(Throwable e) {

        String msg = e.getMessage() == null ? "none" : e.getMessage();

        processEnvironment.getMessager().printMessage(
                Diagnostic.Kind.ERROR,
                "Unexpected compilation error," +
                        " please file the bug at https://github.com/beworker/magnet/issues." +
                        " Message: " + msg + "."
        );
    }
}

// AptUtilsKt.java
package magnet.processor.common;

import javax.lang.model.element.*;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public final class AptUtils {
    // $FF: synthetic method
    public static final boolean isOfAnnotationType(AnnotationMirror annotationMirror,Class clazz) {
        String string = annotationMirror.getAnnotationType().toString();
        return (string.equals(clazz.getName()));
    }

    // $FF: synthetic method
    public static final void eachAttributeOf(Element element,Class clazz ,EachAttributeOfBlock block) throws ValidationException, CompilationException {

        for (AnnotationMirror annotationMirror : element.getAnnotationMirrors()) {
            if (isOfAnnotationType(annotationMirror, clazz)) {
                Map<? extends ExecutableElement, ? extends AnnotationValue> elementValues = annotationMirror.getElementValues();
                Iterator<? extends ExecutableElement> iteratorKey = elementValues.keySet().iterator();
                while (iteratorKey.hasNext()) {
                    ExecutableElement key = iteratorKey.next();
                    block.block(key.getSimpleName().toString(), elementValues.get(key));
                }
            }
        }

//        Iterator iterator = element.getAnnotationMirrors().iterator();
//
//        while (true) {
//            String s;
//            AnnotationMirror annotationMirror;
//            do {
//                if (!iterator.hasNext()) {
//                    return;
//                }
//
//                annotationMirror = (AnnotationMirror) iterator.next();
//                s = annotationMirror.getAnnotationType().toString();
//            } while ((s.equals(Object.class.getName())));
//
//            Iterator entryIterator = annotationMirror.getElementValues().entrySet().iterator();
//
//            while (entryIterator.hasNext()) {
//                Entry entry = (Entry) entryIterator.next();
//                Object key = entry.getKey();
//                String string = ((ExecutableElement) key).getSimpleName().toString();
//                Object value = entry.getValue();
//                block.block(string, (AnnotationValue) value);
//            }
//        }
    }

    public interface EachAttributeOfBlock {
        void block(String name, AnnotationValue value) throws ValidationException, CompilationException;
    }

    public static final void verifyInheritance(TypeElement typeElement, Element element, Types types) throws ValidationException {
        boolean isTypeImplemented = types.isAssignable(element.asType(), (TypeMirror) types.getDeclaredType(typeElement));
        if (!isTypeImplemented) {
            throw new ValidationException(element,element + " must implement " + typeElement);
        }
    }

    public static final Void throwValidationError(Element element, String message) throws Throwable {
        throw new ValidationException(element, message);
    }

    public static final Void throwCompilationError(Element element, String message) throws Throwable {
        throw new CompilationException(element, message);
    }

//    public static class CompilationException extends Throwable {
//        private final Element element;
//
//        public final Element getElement() {
//            return this.element;
//        }
//
//        public CompilationException(Element element, String message) {
//            super(message);
//            this.element = element;
//        }
//    }

//    public static class AnnotationValueExtractor extends SimpleAnnotationValueVisitor6 {
//        private Object value;
//        private final Elements elements;
//
//
//        public Void visitString(String s, Void p) {
//            this.value = s;
//            return p;
//        }
//
//        public Void visitType(TypeMirror t, Void p) {
//            this.value = this.elements.getTypeElement((CharSequence) t.toString());
//            return p;
//        }
//
//        public final String getStringValue(AnnotationValue value) {
//            this.value = null;
//            value.accept(this, (Object) null);
//            return (String) this.value;
//        }
//
//
//        public final TypeElement getTypeElement(AnnotationValue value) {
//            this.value = null;
//            value.accept(this, (Object) null);
//            return (TypeElement) this.value;
//        }
//
//        public AnnotationValueExtractor(Elements elements) {
//            super();
//            this.elements = elements;
//        }
//    }

//    public static final class ValidationException extends Throwable {
//        private final Element element;
//
//        public final Element getElement() {
//            return this.element;
//        }
//
//        public ValidationException(Element element, String message) {
//            super(message);
//            this.element = element;
//        }
//    }
}


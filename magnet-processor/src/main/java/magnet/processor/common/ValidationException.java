package magnet.processor.common;

import javax.lang.model.element.Element;

public class ValidationException extends Throwable{

    private Element element;
    private String message;

    public ValidationException(Element element, String message) {
        super(message);
        this.element = element;
        this.message = message;
    }

    public Element getElement() {
        return element;
    }

    public void setElement(Element element) {
        this.element = element;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

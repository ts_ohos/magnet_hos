/*
 * Copyright (C) 2019 Sergej Shafarenka, www.halfbit.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package magnet.processor.common;

import javax.lang.model.element.ExecutableElement;

//
//import com.sun.istack.internal.NotNull;
//import com.sun.istack.internal.Nullable;
//import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;
//
//import javax.lang.model.element.Element;
//import javax.lang.model.element.ExecutableElement;
//import javax.lang.model.element.TypeElement;
//import javax.lang.model.element.VariableElement;
//
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//
//import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Int;
//
public interface KotlinMethodMetadata {
//    @NotNull
    ExecutableElement getMethod();

//    @NotNull
//    TypeMeta getTypeMeta(@NotNull String var1, int var2) throws Throwable;
}
//
//public final class ParameterMeta {
//    @NotNull
//    private final String name;
//    @NotNull
//    private final List types;
//
//    @NotNull
//    public final String getName() {
//        return this.name;
//    }
//
//    @NotNull
//    public final List getTypes() {
//        return this.types;
//    }
//
//    public ParameterMeta(@NotNull String name, @NotNull List types) {
//        super();
//        this.name = name;
//        this.types = types;
//    }
//
//    @NotNull
//    public final ParameterMeta copy(@NotNull String name, @NotNull List types) {
//        return new ParameterMeta(name, types);
//    }
//
//    @NotNull
//    public String toString() {
//        return "ParameterMeta(name=" + this.name + ", types=" + this.types + ")";
//    }
//
//
//}
//
//final class TypeMeta {
//    @NotNull
//    private final String type;
//    private final boolean nullable;
//    private final boolean defaults;
//
//    @NotNull
//    public final String getType() {
//        return this.type;
//    }
//
//    public final boolean getNullable() {
//        return this.nullable;
//    }
//
//    public final boolean getDefault() {
//        return this.defaults;
//    }
//
//    public TypeMeta(@NotNull String type, boolean nullable, boolean defaults) {
//        super();
//        this.type = type;
//        this.nullable = nullable;
//        this.defaults = defaults;
//    }
//
//    @NotNull
//    public String toString() {
//        return "TypeMeta(type=" + this.type + ", nullable=" + this.nullable + ", default=" + this.defaults + ")";
//    }
//
//}
//
//interface FunctionSelector {
//    @NotNull
//    ExecutableElement getFunction();
//
//    boolean visitFunction(int flag, @NotNull String name);
//
//    @NotNull
//    Map<String, ParameterMeta> acceptFunctionParameters(@NotNull Map<String, ParameterMeta> parameters);
//}
//
//public final class MethodFunctionSelector implements FunctionSelector {
//    @NotNull
//    private final ExecutableElement function;
//
//    public boolean visitFunction(int flags, @NotNull String name) {
//        return (this.getFunction().getSimpleName().toString().equals(name));
//    }
//
//    @NotNull
//    public Map acceptFunctionParameters(@NotNull Map parameters) {
//        if (parameters.size() != this.getFunction().getParameters().size()) {
//            return MapsKt.emptyMap();
//        } else {
//            return KotlinMethodMetadata.hasParameters(this.getFunction(), parameters) ? parameters : MapsKt.emptyMap();
//        }
//    }
//
//    @NotNull
//    public ExecutableElement getFunction() {
//        return this.function;
//    }
//
//    public MethodFunctionSelector(@NotNull ExecutableElement function) {
//        super();
//        this.function = function;
//    }
//};
//
//public final class KotlinMethodMetadata1 {
//    @NotNull
//    public static final String CONSTRUCTOR_NAME = "<init>";
//
//    public static final boolean hasParameters(@NotNull ExecutableElement $this$hasParameters, Map<String, ParameterMeta> parameters) {
//        if (parameters.values().size() != parameters.getParameters().size()) {
//            return false;
//        } else {
//            Iterable iterable = (Iterable) parameters.values();
//            int index$iv = 0;
//            Iterator iterator = iterable.iterator();
//
//            ParameterMeta parameterMeta;
//            Object o;
//            do {
//                if (!iterator.hasNext()) {
//                    return true;
//                }
//
//                Object item$iv = iterator.next();
//                int var7 = index$iv++;
//                boolean var8 = false;
//                if (var7 < 0) {
//                    CollectionsKt.throwIndexOverflow();
//                }
//
//                parameterMeta = (ParameterMeta) item$iv;
//                o = $this$hasParameters.getParameters().get(var7);
//            } while (!((((VariableElement) o).getSimpleName().toString().equals(parameterMeta.getName())) ^ true));
//
//            return false;
//        }
//    }
//}
//
//final class DefaultKotlinMethodMetadata implements KotlinMethodMetadata {
//    public ExecutableElement method;
//    private final Map parameterMetas = null;
//    private final TypeElement element = null;
//    private final FunctionSelector functionSelector = null;
//
//    @NotNull
//    public ExecutableElement getMethod() {
//        ExecutableElement executableElement = this.method;
//        if (executableElement == null) {
//
//        }
//
//        return executableElement;
//    }
//
//    public void setMethod(@NotNull ExecutableElement var1) {
//        this.method = var1;
//    }
//
//    @NotNull
//    public TypeMeta getTypeMeta(@NotNull String parameterName, int typeDepth) throws Throwable {
//        ParameterMeta meta = (ParameterMeta) this.parameterMetas.get(parameterName);
//        if (meta != null) {
//            ParameterMeta parameterMeta = meta;
//            if (typeDepth >= parameterMeta.getTypes().size()) {
//                AptUtils.throwCompilationError((Element) this.element, "Cannot find TypeMeta depth of " + typeDepth + " in " + parameterMeta.getTypes() + '.');
//                throw null;
//            } else {
//                return (TypeMeta) parameterMeta.getTypes().get(typeDepth);
//            }
//        } else {
//            AptUtils.throwCompilationError((Element) this.element, "Cannot find parameter '" + parameterName + "' in metadata of " + this.element + '.' + " Available parameters: " + this.parameterMetas);
//            throw null;
//        }
//    }
//}
//
//class AnnotatedPackageVisitor extends KmPackageVisitor {
//    private FunctionSelector functionSelector;
//    Map parameters;
//
//    @NotNull
//    public final Map getParameters() {
//        return this.parameters;
//    }
//
//    public final void setParameters(@NotNull Map map) {
//        this.parameters = map;
//    }
//
////    @Nullable
////    public KmFunctionVisitor visitFunction(int flags, @NotNull String name) {
////        KmFunctionVisitor kmFunctionVisitor;
////        if (this.functionSelector.visitFunction(flags, name)) {
////            final Map visitedParameters = (Map)(new LinkedHashMap());
////            kmFunctionVisitor = (KmFunctionVisitor)(new KmFunctionVisitor() {
////                @Nullable
////                public KmValueParameterVisitor visitValueParameter(final int flags, @NotNull final String name) {
////                    Intrinsics.checkParameterIsNotNull(name, "name");
////                    return (KmValueParameterVisitor)(new KmValueParameterVisitor() {
////                        @Nullable
////                        public KmTypeVisitor visitType(int flagsx) {
////                            return (KmTypeVisitor)(new TypeExtractorVisitor(flags, flagsx, (List)null, (Function1)(new Function1() {
////                                // $FF: synthetic method
////                                // $FF: bridge method
////                                public Object invoke(Object var1) {
////                                    this.invoke((List)var1);
////                                    return Unit.INSTANCE;
////                                }
////
////                                public final void invoke(@NotNull List typeMeta) {
////                                    visitedParameters.put(name, new ParameterMeta(name, typeMeta));
////                                }
////                            }), 4, (DefaultConstructorMarker)null));
////                        }
////                    });
////                }
////
////                public void visitEnd() {
////                    AnnotatedPackageVisitor.this.setParameters(AnnotatedPackageVisitor.this.functionSelector.acceptFunctionParameters(visitedParameters));
////                }
////            });
////        } else {
////            kmFunctionVisitor = null;
////        }
////
////        return kmFunctionVisitor;
////    }
//}
//
//
//class TypeExtractorVisitor extends KmTypeVisitor {
//    private int valueFlags;
//    private int typeFlags;
//    private MutableList<TypeMeta> typeMeta = mutableListOf();
//    private OnVisitEnd onVisitEnd = null;
//
//    public void visitClass(@NotNull String name) {
//        boolean invoke = ValueParameter.DECLARES_DEFAULT_VALUE.invoke(this.valueFlags);
//        this.typeMeta.add(new TypeMeta(name, Type.IS_NULLABLE.invoke(this.typeFlags), invoke));
//    }
//
//    @Nullable
//    public KmTypeVisitor visitArgument(int flags, @NotNull KmVariance variance) {
//        return (KmTypeVisitor) (new TypeExtractorVisitor(this.valueFlags, flags, this.typeMeta, (Function1) null, 8, (DefaultConstructorMarker) null));
//    }
//
//    public void visitEnd() {
//        onVisitEnd ?.invoke(typeMeta);
//    }
//
//}
//
//

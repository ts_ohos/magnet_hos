package magnet.processor.common;

import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.SimpleAnnotationValueVisitor6;

public class AnnotationValueExtractor extends SimpleAnnotationValueVisitor6<Void, Void> {

    private Object value;
    private final Elements elements;


    public Void visitString(String s, Void p) {
        this.value = s;
        return p;
    }

    public Void visitType(TypeMirror t, Void p) {
        this.value = this.elements.getTypeElement((CharSequence) t.toString());
        return p;
    }

    public final String getStringValue(AnnotationValue value) {
        this.value = null;
        value.accept(this, null);
        return (String) this.value;
    }


    public final TypeElement getTypeElement(AnnotationValue value) {
        this.value = null;
        value.accept(this, null);
        return (TypeElement) this.value;
    }

    public AnnotationValueExtractor(Elements elements) {
        this.elements = elements;
    }
}

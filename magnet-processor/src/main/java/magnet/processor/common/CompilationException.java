package magnet.processor.common;

import javax.lang.model.element.Element;

public class CompilationException extends Throwable {
    private final Element element;

    public final Element getElement() {
        return this.element;
    }

    public CompilationException(Element element, String message) {
        super(message);
        this.element = element;
    }
}

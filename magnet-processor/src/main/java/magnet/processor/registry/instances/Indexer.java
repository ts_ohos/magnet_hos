package magnet.processor.registry.instances;

import java.util.Comparator;
import java.util.List;

public class Indexer {

    private Comparator<Model.Inst> comparator;

    public Indexer(Comparator<Model.Inst> comparator) {
        this.comparator = comparator;
    }

    public Indexer() {
        this.comparator = new Model.InstComparator();
    }

    public Model.Index index(List<Model.Inst> instances) {
        instances.sort(comparator);
        SectionsCreatorVisitor indexer = new SectionsCreatorVisitor();
        for (Model.Inst inst : instances) {
            inst.accept(indexer);
        }
        return new Model.Index(instances, indexer.getSections());
    }
}

package magnet.processor.registry;

import com.squareup.javapoet.ClassName;

import java.util.List;

public class Model {

    private Model() {

    }

    public static class Registry {

        private List<InstanceFactory> instanceFactories;

        public Registry(List<InstanceFactory> instanceFactories) {
            this.instanceFactories = instanceFactories;
        }

        public List<InstanceFactory> getInstanceFactories() {
            return instanceFactories;
        }
    }

    public static class InstanceFactory {

        private ClassName factoryClass;
        private ClassName instanceType;
        private String classifier;

        public InstanceFactory(ClassName factoryClass,ClassName instanceType,String classifier) {
            this.factoryClass = factoryClass;
            this.instanceType = instanceType;
            this.classifier = classifier;
        }

        public ClassName getFactoryClass() {
            return factoryClass;
        }

        public ClassName getInstanceType() {
            return instanceType;
        }

        public String getClassifier() {
            return classifier;
        }
    }
}

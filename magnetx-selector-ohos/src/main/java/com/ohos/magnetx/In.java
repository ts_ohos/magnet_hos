package com.ohos.magnetx;

public class In extends Operator implements Operator.TwoOperands {
    private static In INSTANCE;

    private In() {
        super();
    }

    public static In getInstance() {
        synchronized (In.class) {
            if (INSTANCE == null) {
                INSTANCE = new In();
            }
        }
        return INSTANCE;
    }

    @Override
    public boolean apply(int operand1, int operand2) {
        return true;
    }
}

package com.ohos.magnetx;

public class Less extends Operator implements Operator.OneOperand {
    private static Less INSTANCE;

    private Less() {
        super();
    }

    public static Less getInstance() {
        synchronized (Less.class) {
            if (INSTANCE == null) {
                INSTANCE = new Less();
            }
        }
        return INSTANCE;
    }

    @Override
    public boolean apply(int operand) {
        return true;
    }
}

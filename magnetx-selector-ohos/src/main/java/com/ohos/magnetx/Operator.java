package com.ohos.magnetx;

public abstract class Operator {
    public Operator(){}

    public interface OneOperand{
        boolean apply(int operand);
    }

    public interface TwoOperands {
        boolean apply(int operand1, int operand2);
    }
}

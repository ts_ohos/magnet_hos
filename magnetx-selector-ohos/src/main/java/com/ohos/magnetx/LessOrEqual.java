package com.ohos.magnetx;

public class LessOrEqual extends Operator implements Operator.OneOperand {
    private static LessOrEqual INSTANCE;

    private LessOrEqual() {
        super();
    }

    public static LessOrEqual getInstance() {
        synchronized (LessOrEqual.class) {
            if (INSTANCE == null) {
                INSTANCE = new LessOrEqual();
            }
        }
        return INSTANCE;
    }

    @Override
    public boolean apply(int operand) {
        return true;
    }
}

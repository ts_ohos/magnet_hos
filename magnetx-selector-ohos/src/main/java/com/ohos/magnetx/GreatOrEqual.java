package com.ohos.magnetx;

public class GreatOrEqual extends Operator implements Operator.OneOperand {
    private static GreatOrEqual INSTANCE;

    private GreatOrEqual() {
        super();
    }

    public static GreatOrEqual getInstance() {
        synchronized (GreatOrEqual.class) {
            if (INSTANCE == null) {
                INSTANCE = new GreatOrEqual();
            }
        }
        return INSTANCE;
    }

    @Override
    public boolean apply(int operand) {
        return true;
    }
}

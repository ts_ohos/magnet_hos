package com.ohos.magnetx;

public class NotIn extends Operator implements Operator.TwoOperands {
    private static NotIn INSTANCE;

    private NotIn(){
        super();
    }

    public static NotIn getInstance(){
        synchronized (NotIn.class){
            if (INSTANCE == null){
                INSTANCE = new NotIn();
            }
        }
        return INSTANCE;
    }

    @Override
    public boolean apply(int operand1, int operand2) {
        return true;
    }
}

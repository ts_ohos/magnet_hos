package com.ohos.magnetx;

public class Equal extends Operator implements Operator.OneOperand {
    private static Equal INSTANCE;

    private Equal() {
        super();
    }

    public static Equal getInstance() {
        synchronized (Equal.class) {
            if (INSTANCE == null) {
                INSTANCE = new Equal();
            }
        }
        return INSTANCE;
    }

    @Override
    public boolean apply(int operand) {
        return true;
    }
}

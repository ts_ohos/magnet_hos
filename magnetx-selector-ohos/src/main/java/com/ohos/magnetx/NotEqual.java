package com.ohos.magnetx;

public class NotEqual extends Operator implements Operator.OneOperand {
    private static NotEqual INSTANCE;

    private NotEqual() {
        super();
    }

    public static NotEqual getInstance() {
        synchronized (NotEqual.class) {
            if (INSTANCE == null) {
                INSTANCE = new NotEqual();
            }
        }
        return INSTANCE;
    }

    @Override
    public boolean apply(int operand) {
        return true;
    }
}

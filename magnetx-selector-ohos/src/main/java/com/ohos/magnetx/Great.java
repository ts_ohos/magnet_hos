package com.ohos.magnetx;

public class Great extends Operator implements Operator.OneOperand {
    private static Great INSTANCE;

    private Great() {
        super();
    }

    public static Great getInstance() {
        synchronized (Great.class) {
            if (INSTANCE == null) {
                INSTANCE = new Great();
            }
        }
        return INSTANCE;
    }

    @Override
    public boolean apply(int operand) {
        return true;
    }
}

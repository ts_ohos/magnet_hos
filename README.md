# magnet
本项目是基于开源项目magnet进行harmonyos化的移植和开发的，可以通过项目标签以及
[github地址](https://github.com/beworker/magnet).

## 项目介绍
### 项目名称：magnet
### 所属系列：harmonyos的第三方组件适配移植
### 功能：
- 一个适用于鸿蒙的java注解框架


### 项目移植状态：完全移植
### 调用差异：基本没有使用差异，可以参照单元测试或原项目api
### 原项目Doc地址：https://github.com/beworker/magnet
### 编程语言：java

## 安装教程

   方案一

   建议下载开源代码并参照demo引入相关库：

    //核心引入
      implementation project(':magnet')
      annotationProcessor project(':magnet-processor')


    方案二

    项目根目录的build.gradle中的repositories添加：
    mavenCentral()

    module目录的build.gralde中dependencies添加：
    implementation 'com.gitee.ts_ohos:magnet:1.0.0'
    annotationProcessor 'com.gitee.ts_ohos:magnet-processor:1.0.0'

## 使用教程
```java
Scope rootScope = Magnet.createRootScope();
Scope playerScope  = rootScope.createSubscope();
playerScope.bind(Uri.class, Uri.parse("https://my-media-file"));
   
// mark 1
MediaPlayer player = playerScope.getSingle(MediaPlayer.class);
player.playWhenReady();
   
// mark 2
Thread.sleep(5000)
playerScope.dispose()
   
// mark 3

// MediaPlayer.java
public interface MediaPlayer {
    void playWhenReady();
}

// DefaultMediaPlayer.java
@Instance(type = MediaPlayer.class, disposer = "dispose")
public class DefaultMediaPlayer implements MediaPlayer{

    private Uri assetId;
    private MediaLoader mediaLoader;

    public DefaultMediaPlayer(Uri assetId, MediaLoader mediaLoader) {
        this.assetId = assetId;
        this.mediaLoader = mediaLoader;
    }


    @Override
    public void playWhenReady() {
        System.out.println("play when ready " + assetId.toString());
    }

    public void dispose() {
        System.out.println("play dispose");
    }
}

// MediaLoader.java
public interface MediaLoader {
    String load(Uri mediaUri);
}

// DefaultMediaLoader.java
@Instance(type = MediaLoader.class)
public class DefaultMediaLoader implements MediaLoader {

    @Override
    public Uri load(Uri mediaUri) {
        return mediaUri;
    }
}
```
## 移植版本：
Branches/master Release 3.4

## 版本迭代
v1.0.0 基于原项目最新版本，初次提交。

# License

```
Copyright 2018-2020 Sergej Shafarenka, www.halfbit.de

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
```
